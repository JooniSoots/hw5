import java.util.*;
/* Viited:
 * http://enos.itcollege.ee/~jpoial/algoritmid/TreeNode.java
 * https://git.wut.ee/i231/home5/src/master/src/Node.java
 * http://www.geeksforgeeks.org/expression-tree/
 */
public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;
   
   public Tnode(){
   }
   /*public Tnode(String symbol){
	   name = symbol;
   }*/
   public void setName(String s) {
	      this.name = s;
	   }

   public String getName() {
	      return this.name;
	   }
   public void setNextSibling (Tnode p) {
	      this.nextSibling = p;
	   }

   public Tnode getNextSibling() {
	      return this.nextSibling;
	   }

   public void setFirstChild (Tnode a) {
	      this.firstChild = a;
	   }

   public Tnode getFirstChild() {
	      return this.firstChild;
	   }
   public boolean hasSibling() {
	      return (getNextSibling() != null);
	   }
   public boolean hasChild() {
	      return (getFirstChild() != null);
	   }   
   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      // TODO!!! esitab RPN vasakpoolse suluesitusena.
      b.append(getName());
      if (getFirstChild() != null) {
			b.append("(");
			b.append(getFirstChild().toString());
		}
		if (getNextSibling() != null) {
			b.append(",");
			b.append(getNextSibling().toString());
			b.append(")");
		}    
      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
	   if (pol == null || pol.length() == 0)
			throw new RuntimeException("RPN loendi sisend on tyhi.");
      Tnode root = null;
      // TODO!!!
	   	Stack<Tnode>loend2 = new Stack<Tnode>();
		StringTokenizer tk = new StringTokenizer(pol, " \t");
		Tnode l1;
		Tnode l2;
		
		int i = 1;
		int loendur = tk.countTokens();
			
		while (tk.hasMoreElements()) {
				
			String symbol = (String) tk.nextElement();
				
			if (symbol.equals("-") || symbol.equals("+") || symbol.equals("/") || symbol.equals("*")) {	
				if (loend2.size() == 0){
					throw new RuntimeException("RPN loendis [" + pol + "] ei ole elemente tehte teostamiseks"); 
				}
				
				else if (loend2.size() == 1){
					throw new RuntimeException("RPN loendis [" + pol + "] on alles yks element, mis on liiga vahe viimase tehte \"" + symbol  + "\" teostamiseks");
				}else{                       
				root = new Tnode();
				root.setName(symbol);
				l1 = loend2.pop();
				l2 = loend2.pop();
				root.setNextSibling(l1);
				root.setFirstChild(l2);
				loend2.push(root);
				}
		 	} else{
					if (loendur == i && i > 1) {
						throw new RuntimeException("RPN loendis [" + pol + "] on yleliigseid symboleid.");
					}
					try{
					    double a = Double.parseDouble(symbol);
					    Tnode number = new Tnode();
					    number.setName(symbol);
					    loend2.push(number);
					}
					catch(RuntimeException e) {           
					    throw new RuntimeException("RPN loendis ["+ pol + "] olev symbol " + symbol +" ei sobi RPN meetodi teostamiseks."); 
					}  
				}
				i++;
			}      
		root = loend2.pop();
		return root;
   }

   public static void main (String[] param) {
      String rpn = "1 2 +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      // TODO!!! Your tests here
   }
}

